

<div class="container">
  <h2>Manage Easy Scheduling System</h2>
  <div class="list-group">
    <?php foreach($content as  $list): ?>  
    <div class="list-group-item">
      <h4 class="list-group-item-heading"><?php print $list['link'];?>  </h4>
        <p class="list-group-item-text"> <?php print $list['description'];?> </p>
    </div>
    
    <?php endforeach; ?>
  </div>
</div>