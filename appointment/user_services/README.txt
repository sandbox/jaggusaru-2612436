Appointment is a highly customizable module that allows your customers to book appointments with you via the web.

Features
The module was designed to be flexible and reliable so as to be able to meet the needs of any kind of enterprise. You can the read the main features of the system below:

- Full customers and appointments management.
- Services and service providers organization.
- Work flow and booking rules.
- Email notifications system.

Installation
. Download the module code to the /modules directory within your Drupal installation.
At admin/modules, enable the modules(services,Provider,appointment). That's it.

Following are the urls which are created when modules are enabled:
- Add services 
	http://[yoursite.com]/appointment/services/add
- List Services
	http://[yoursite.com]/appointment/service_lists
- Add provider
	http://[yoursite.com]/appointment/provider/add
- List Provider
		http://[yoursite.com]/appointment/provider_lists
- Book an Appointment
	http://[yoursite.com]/appointment/book
- List appointment
	http://[yoursite.com]/appointment/lists


Future Development
- Google Calendar synchronization.

Whether it is new ideas or defects, your feedback is highly appreciated and will be taken into consideration for the following releases of the project. Share your experience and discuss your thoughts with other users through communities. Create issues with suggestions on new features or bug reports