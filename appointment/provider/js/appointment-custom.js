
//latestJquery('#edit-monday-start').timepicker();
(function($) {
	Drupal.behaviors.appointmentModule = {
		attach: function (context, settings) {	
//alert(typeof $.fn.attr === 'function');
$('#edit-monday-start').timepicker({'scrollDefault': 'now'});
//$("#edit-provider-registration-detail1,#edit-break-start,#edit-break-end,#edit-tuesday-break-start,#edit-tuesday-break-end,#edit-wednesday-break-start,#edit-wednesday-break-end,#edit-thursday-break-start,#edit-thursday-break-end,#edit-friday-break-start,#edit-friday-break-end,#edit-saturday-break-start,#edit-saturday-break-end,#edit-sunday-break-start,#edit-sunday-break-end,#edit-monday-start,#edit-monday-end,#edit-tuesday-start,#edit-tuesday-end,#edit-wednesday-start,#edit-wednesday-end,#edit-thursday-start,#edit-thursday-end,#edit-friday-start,#edit-friday-end,#edit-saturday-start,#edit-saturday-end,#edit-sunday-start,#edit-sunday-end").timepicker({ 'scrollDefault': 'now' });

	$('#datepicker').datepicker({
             dateFormat: 'dd-mm-yy',
             firstDay: 1, // Monday
             minDate: 0,
            //defaultDate: Date.today(),
            inline: true,
            showOtherMonths: true,
            dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
        onSelect: function(dateText, instance) {
			getAvailableHours(dateText);
			updateConfirmFrame();
		}
        });
	/*	
    jQuery('#edit-provider').change(function(){
         var providerId = jQuery('#edit-provider').val();
         var url = drupalSettings.path.baseUrl + 'appointment/getProviderDescription/'+ providerId;
        jQuery.ajax(
            {
                url: url,
                type: 'GET',
                success: function(data){
                alert(data);
                }
            }
        );

    });
	*/
	
	$('#available-hours .available-hour').live('click',function() {
		
            $('.selected-hour').removeClass('selected-hour');
            $(this).addClass('selected-hour');

            updateConfirmFrame();
        });
		
	function updateConfirmFrame(){
		var startDatetime = $('#datepicker').datepicker('getDate').toString('yyyy-MM-dd')
                                    + ' ' + $('.selected-hour').text() + ':00';
									//alert(startDatetime);
		 $('#start_datetime').val(startDatetime);
		calculateEndDate();
	}
	function calculateEndDate(){
		var serviceDuration = $('#service_duration').val();
		var startDatetime = $('#datepicker').datepicker('getDate').toString('dd-MM-yyyy') + ' ' + $('.selected-hour').text();
		//alert(Date.parse($startDatetime));
		startDatetime = Date.parseExact(startDatetime, 'dd-MM-yyyy HH:mm');
		//alert(startDatetime);
		 if(startDatetime !== null) {
           endDatetime = startDatetime.add({ 'minutes' : parseInt(serviceDuration) });
			//endDatetime = startDatetime.setMinutes(startDatetime.getMinutes() + parseInt(serviceDuration));
			
        } else {
            endDatetime = new Date();
			
        }
		//alert(endDatetime.toString('yyyy-MM-dd HH:mm:ss'));
       $('#end_datetime').val(endDatetime.toString('yyyy-MM-dd HH:mm:ss'));
    }
   function  getAvailableHours(dateText){
       var postData = {
           //'service_id': $('#select-service').val(),
           'provider_id': $('.provider_id').val(),
           'selected_date': dateText,
          'service_duration': $('#service_duration').val()
           //'manage_mode': FrontendBook.manageMode,
          // 'appointment_id': appointmentId
      };
//drupalSettings.path.baseUrl + 
       var url = 'http://localhost/drupal-7.40/appointment/getWorkingHours';

       $.ajax({
               url: url,
               data:postData,
                method: 'POST',
               dataType: 'json',
               success:function(response){
				   
                   if (response.length > 0) {
                       var currColumn = 1;
                       $('#available-hours').html('<div style="width:50px; float:left;"></div>');

                       $.each(response, function(index, availableHour) {
                           if ((currColumn * 10) < (index + 1)) {
                               currColumn++;
                               $('#available-hours').append('<div style="width:50px; float:left;"></div>');
                           }

                           $('#available-hours div:eq(' + (currColumn - 1) + ')').append(
                               '<span class="available-hour selected-hour">' + availableHour + '</span><br/>');
                           $('.available-hour').hover(function(){
                               $(this).css({'background-color': '#caedf3','cursor':'pointer','font-weight':'bold'});
                           }, function(){
                               $(this).css({'background-color': '#EFEFEF','font-weight':'normal'});

                           });
                           $('.available-hour:eq(0)').css({'color': '#0088cc','font-weight': 'bold','text-decoration': 'underline'});

                           $('.available-hour').live('click',function(){
                               $('.available-hour').css({'background-color': '#EFEFEF','font-weight':'normal'});
                               // alert('f');
                               $(this).css({'background-color': '#caedf3','cursor':'pointer','font-weight':'bold'});
                           });
                       });


					} else {
                       $('#available-hours').text('There are no available appointment hours for the selected date. Please choose another date.');
                   }
               }
            });
   }
   /* Change active class for progress bar' */
   var step = $('#step-value').val();
    $('#progressbar li').each(function(i){
		while(i < step){
			$(this).addClass('active');
			i++;
		}
   });

   $('.editBreak').click(function(){
		alert('hi');
	});
	
	}
};		
})(jQuery);

