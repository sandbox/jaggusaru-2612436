<?php
/**
* Provider update form
*/
function provider_update_form($form, &$form_state)
{
	$provider = is_provider_id_set();
	$form = [];
	$form['provider_tab'] =[
		'#type' => 'vertical_tabs',
	];
	
	$form['provider'] = [
		'#type' => 'fieldset',
		'#title' => t('Provider\'s Details'),
		'#group' => 'provider_tab',
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
	];
	
	 
	$form['working_plan_break_fieldset'] = [
		'#type' => 'fieldset',
		'#title' => t('Break Details'),
		'#group' => 'provider_tab',
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
		
	];
	$form['provider_working_plan'] = [
		'#type' => 'fieldset',
		'#title' => t('Working Plan'),
		'#group' => 'provider_tab',
		];
	$form['provider']['first_name']=[
		'#type' => 'textfield',
		'#title' => t('First Name'),
		'#required' => true,
		'#default_value' => $provider->first_name,
		'#max_length' => 128,
		//'#prefix' => '<table><tr><td>',
		//'#suffix' => '</td>',
		'#group' => 'provider_tab'
		];
	$form['provider']['middle_name']=[
		'#type' => 'textfield',
		'#title' => t('Middle Name'),
		//'#required' => true,
		'#default_value' => !empty($provider->middle_name)? $provider->middle_name :'',
		'#max_length' => 128,
		//'#prefix' => '<td>',
		//'#suffix' => '</td></tr></table>',
		//'#group' => 'provider_tab'
		];
		
	$form['provider']['last_name'] = [
		'#type' => 'textfield',
		'#title' => t('Last Name'),
		'#required' => true,
		'#default_value' => $provider->last_name,
		];
	$form['provider']['mobile'] = [
		'#type' => 'textfield',
		'#title' => t('Mobile'),
		'#required' => true,
		'#default_value' => $provider->mobile,
		];
	$form['provider']['landline'] = [
		'#type' => 'textfield',
		'#title' => t('Landline'),
		//'#required' => true,
		'#default_value' => !empty($provider->landline)? $provider->landline :'',
		];
	$form['provider']['address'] = [
		'#type' => 'textfield',
		'#title' => t('Address'),
		'#required' => true,
		'#default_value' => $provider->address,
		];
	
	$form['provider']['city'] = [
		'#type' => 'textfield',
		'#title' => t('City'),
		'#required' => true,
		'#default_value' => $provider->city,
		];
	$form['provider']['state'] = [
		'#type' => 'textfield',
		'#title' => t('State'),
		'#required' => true,
		'#default_value' => $provider->state,
		];
	$form['provider']['zipcode'] = [
		'#type' => 'textfield',
		'#title' => t('Zip Code'),
		'#required' => true,
		'#default_value' => $provider->zipcode,
		
		];
	$form['provider']['email'] = [
		'#type' => 'textfield',
		'#title' => t('Your e-mail address'),
		'#maxlength' => 255,
		'#default_value' => $provider->email,
		
		];
	$form['provider']['note'] = [
		'#type' => 'textarea',
		'#title' => t('Write your note'),
		'#maxlength' => 255,
		'#default_value' => $provider->note,
		
		];
	//$service_options = !empty(get_service_by_service_id($provider->service_id)
	$form['provider']['service_id'] = [
		'#type' => 'select',
		'#title' => t('Service'),
		//'#required' => true,
		'#options' => services_load_all_services(),
		'#default_value' => !empty(get_service_by_service_id($provider->service_id)) ? get_service_by_service_id($provider->service_id) : services_load_all_services(),
		
		];
	
	$form['provider']['id']= ['#type' =>'hidden', '#value' => $provider->id];
	$form['provider_working_plan']['fieldset']['day'] = array(
            '#type' => 'markup',
            '#markup' =>t('Day'),
           // '#group' => 'vertical_tabs',
            '#prefix'=>'<table><thead><tr><th>',
            '#suffix' => '</th>',
        );
	$form['provider_working_plan']['fieldset']['start'] = array(
		'#type' => 'markup',
		'#markup' =>t('Start'),
		//'#group' => 'vertical_tabs',
		'#prefix' => '<th>',
		'#suffix' => '</th>',
	);

	$form['provider_working_plan']['fieldset']['end'] = array(
		'#type' => 'markup',
		'#markup' =>t('End'),
	   // '#group' => 'vertical_tabs',
		'#prefix' => '<th>',
		'#suffix' => '</th></tr></thead><tr>',
	);
		$workingPlan = json_decode($provider->working_plan,true);
		foreach($workingPlan as $key => $value)
		{
			$form['provider_working_plan']['fieldset'][$key.'_label'] = array(
			'#type' => 'markup',
			'#markup' =>ucfirst($key),
			'#prefix' => '<td>',
			'#suffix' => '</td>',
			);
			$form['provider_working_plan']['fieldset'][$key] = array(
			'#type' => 'hidden',
			'#value' =>$key,
			//'#prefix' => '<td>',
			//'#suffix' => '</td>',
			);
			$form['provider_working_plan']['fieldset'][$key.'_start'] = array(
				'#type' => 'textfield',
				'#default_value' => $value['start'],// $form_state->hasValue($k.'_start') ? $form_state->getValue($k.'_start') : '',
				'#prefix' => '<td>',
				'#suffix' => '</td>',
			);
			$form['provider_working_plan']['fieldset'][$key.'_end'] = array(
				'#type' => 'textfield',
				'#default_value' =>  $value['end'],//$form_state->hasValue($k.'_end') ? $form_state->getValue($k.'_end') : '',
			   '#prefix' => '<td>',
				'#suffix' => '</td></tr><tr>'
				);
		}
		$form['provider_working_plan']['fieldset']['advanced_book_timeout'] = array(
			'#type'=>'textfield',
			'#default_value' =>  $provider->advanced_book_timeout,//isset($form_state['values']['advanced_book_timeout']) ? $form_state['values']['advanced_book_timeout'] : '',
			'#title' =>'Timeout ( In Minutes)',
			'#description' => 'Define the timeout (in minutes) before the customers can book or re-arrange appointments with the company. ',
			'#prefix' => '<td>',
			'#suffix' => '</td></tr></table>'
	   );
	
	//$form['working_plan_break_fieldset']['test']=['#type' => 'textfield', '#title' => 'test'];
	
	 $form['working_plan_break_fieldset']['day_header'] = array(
            '#type' => 'markup',
            '#markup' =>t('Day'),
            '#prefix'=>'<table><thead><tr><th>',
            '#suffix' => '</th>',
        );
	$form['working_plan_break_fieldset']['start_header'] = array(
		'#type' => 'markup',
		'#markup' =>t('Start'),
		'#default_value' => '',
		'#prefix' => '<th>',
		'#suffix' => '</th>',
	);

	$form['working_plan_break_fieldset']['end_header'] = array(
		'#type' => 'markup',
		'#markup' =>t('End'),
		'#default_value' => '',
		'#prefix' => '<th>',
		'#suffix' => '</th></tr></thead><tr>',
	);
	$form['working_plan_break_fieldset']['break_day'] = array(
		'#type' => 'select',
		'#options'=>getWeekDays(),
		//'#default_value' => 'monday',
		'#required' => true,
		'#prefix' => '<td>',
		'#suffix' => '</td>',
	);
	$form['working_plan_break_fieldset']['break_start'] = array(
		'#type' => 'textfield',
		'#description' => t('Ex. 2:30pm'),
		'#required' => true,
		'#prefix' => '<td>',
		'#suffix' => '</td>',
	);
	$form['working_plan_break_fieldset']['break_end'] = array(
		'#type' => 'textfield',
		'#description' => t('Ex. 3:00pm'),
		'#required' => true,
		'#prefix' => '<td>',
		'#suffix' => '</td></tr><tr>',
	);
	$form['working_plan_break_fieldset']['break_index_id']=[
		'#type' => 'hidden',
		'#default_value' =>'',
		'#attributes' =>['class' =>'working-plan-break-index-id'],
	];
	$form['working_plan_break_fieldset']['submitbreakdata'] = array(
		'#type' => 'submit',
		'#value' => 'Submit',
		'#name' => 'submit-break',
		'#submit' => ['provider_submit_break_data'],
		'#prefix' => '<td>',
		'#suffix' => '</td>',
		'#ajax' => array(
			'wrapper'=>'provider_working_plan_break',
			'callback' => 'provider_working_plan_ajax_callback'
		)
	);	/*'onclick' => 'return (false);',*/
	$form['working_plan_break_fieldset']['break_data_reset'] = array(
		 '#type' => 'button',
		 '#executes_submit_callback' => FALSE,
		'#attributes' => array('id' =>'break-data-reset-button'),
		'#value' => t('Cancel'),
		'#prefix' => '<td>',
		'#suffix' => '</td></tr></table>',
	);	
	
		$rows = [];
		foreach($workingPlan as $day => $breaks ){
			foreach($breaks['breaks'] as $index => $break){
			//$link = l(t('Edit'),'provider/'.$provider->id.'/'.$day.'/'.$key.'/ajax/edit',['attributes' => ['class' =>'use-ajax']]);
			//$del_link = l(t('Delete'),'provider/'.$provider->id.'/'.$day.'/'.$key.'/ajax/delete',['attributes' => ['class' =>'use-ajax']]);
			//$link = l(t('Edit'),'provider/'.$provider->id.'/'.$day.'/'.$index.'/edit');
			//$link = l(t('Edit'),'',['attributes' =>['class' =>'edit-work-break'],'html' => TRUE]);
			$link = l('Edit', '', array('fragment' => ' ', 'html' => TRUE, 'external' => TRUE,'attributes' =>array('class'=>'edit-work-break')));//l(t('Refresh'), '', array('attributes' => array('class' => 'edit-work-break'), 'fragment' => '','html' => TRUE,'external' => TRUE));
			$del_link = l(t('Delete'),'provider/'.$provider->id.'/'.$day.'/'.$index.'/delete');
			
			$break_index_id = '<input type="hidden" "value"="'.$index.'" class="break-index-id">';
			$rows[] =[ucfirst($day),$break['break_start'],$break['break_end'], $link,$del_link,$break_index_id];
			
			}
		}
	$form['working_plan_break_fieldset']['breaksData']= array(
		'#tree' => TRUE,
		  '#theme' => 'table',
		  '#header' => ['Day','Start','End','Link'],
		  '#rows' => $rows,//array( array('test', 'test'),array('tt','bb')),
		  '#prefix' =>'<div id="provider_working_plan_break">',
		'#suffix' =>'</div>'
		
	);
	$form['submit'] = [
		'#type' => 'submit',
		'#value' => t('Submit'),
		//'#name' => 'provider_add_form',
		//'#submit' =>'provider_add_form_submit_handler',
		
		];	
		return $form;
	}

/**
 * @param $form
 * @param $form_state
 */
function provider_submit_break_data($form,&$form_state){
	
	$break_day = $form_state['values']['break_day'] ? $form_state['values']['break_day'] : '';
	$break_start = $form_state['values']['break_start'] ? $form_state['values']['break_start'] : '';
	$break_end = $form_state['values']['break_end'] ? $form_state['values']['break_end'] : '';
	$break_index_id = isset($form_state['values']['break_index_id']) ? $form_state['values']['break_index_id'] : '' ;
	$provider_data = provider_load_provider_by_id($form_state['values']['id']);
	$working_plan_data = json_decode($provider_data->working_plan,true);
	if(!empty($break_index_id)){
		$working_plan_data[$break_day]['breaks'][$break_index_id] = ['break_start' => $break_start , 'break_end'=>$break_end];
	}else{
		foreach($working_plan_data as $day => $breaks){
		if($day == $break_day){
			$working_plan_data[$day]['breaks'][] = ['break_start' => $break_start , 'break_end'=>$break_end];
			}
		}
	}
	$values = ['working_plan' => json_encode($working_plan_data)];
	try{
		db_update('provider')
			->fields($values)
			->condition('id',$form_state['values']['id'], '=')
			->execute();
			drupal_set_message('Data  updated successfully');
	}catch(PDOException $e){
		drupal_set_message('Data could not be updated');
	}
	
}

/**
 * @param $form
 * @param $form_state
 * @return mixed
 */
function provider_working_plan_ajax_callback($form, &$form_state){
	
	$provider_data = provider_load_provider_by_id($form_state['values']['id']);
	$working_plan_data = json_decode($provider_data->working_plan,true);
	foreach($working_plan_data as $day => $breaks ){
		foreach($breaks['breaks'] as $index => $break){
		//$link = l(t('Edit'),'provider/'.$provider->id.'/'.$day.'/'.$key.'/ajax/edit',['attributes' => ['class' =>'use-ajax']]);
		//$del_link = l(t('Delete'),'provider/'.$provider->id.'/'.$day.'/'.$key.'/ajax/delete',['attributes' => ['class' =>'use-ajax']]);
		//$link = l(t('Edit'),'provider/'.$provider->id.'/'.$day.'/'.$index.'/edit',['attributes' => ['class' =>'edit-work-break']]);
		$link = l(t('Edit'),'#',['attributes' => ['class' =>'edit-work-break']]);
		$del_link = l(t('Delete'),'provider/'.$provider_data->id.'/'.$day.'/'.$index.'/delete',['attributes' => ['class' =>'delete-work-break']]);
		$rows[] =[ucfirst($day),$break['break_start'],$break['break_end'], $link,$del_link];
		}
	}
	$output = array(
	  '#tree' => TRUE,
	  '#theme' => 'table',
	  '#header' => [t('Day'),t('Start'),t('End'),t('Link')],
	  '#rows' => $rows,
	  '#empty' => 'Empty row.',
		
	);
	 $form['working_plan_break_fieldset']['breaksData']['#markup'] = drupal_render($output);
	 return $form['working_plan_break_fieldset']['breaksData'];
	}